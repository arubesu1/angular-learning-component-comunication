import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';

import { IProduct } from './product';
import { ProductService } from './product.service';
import { CriteriaComponent } from '../shared/criteria/criteria.component';
import { PropertyBagService } from './property-bag.service';

@Component({
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit, AfterViewInit {
    @ViewChild(CriteriaComponent) filterComponent: CriteriaComponent;
    pageTitle: string;
    listFilter: string;
    includeDetail: boolean = true;


    imageWidth: number = 50;
    imageMargin: number = 2;
    errorMessage: string;

    filteredProducts: IProduct[];
    products: IProduct[];

    get showImage(): boolean {
        return this.propertyBagService.showImage;
    }

    set showImage(value: boolean) {
        this.propertyBagService.showImage = value;
    }



    ngAfterViewInit(): void {
        this.listFilter = this.filterComponent.listFilter;
    }

    constructor(private productService: ProductService,
        private propertyBagService: PropertyBagService) { }

    ngOnInit(): void {
        this.productService.getProducts().subscribe(
            (products: IProduct[]) => {
                this.products = products;
                // this.performFilter(this.listFilter);
                this.filterComponent.listFilter = this.propertyBagService.filterBy; 
            },
            (error: any) => this.errorMessage = <any>error
        );
    }


    onValueChange(value: string) {
        this.propertyBagService.filterBy = value;
        this.performFilter(value);
    }

    toggleImage(): void {
        this.showImage = !this.showImage;
    }

    performFilter(filterBy?: string): void {
        if (filterBy) {
            this.filteredProducts = this.products.filter((product: IProduct) =>
                product.productName.toLocaleLowerCase().indexOf(filterBy.toLocaleLowerCase()) !== -1
            );
        } else {
            this.filteredProducts = this.products;
        }
    }
}
