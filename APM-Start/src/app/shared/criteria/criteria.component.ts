import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'pm-criteria',
  templateUrl: './criteria.component.html',
  styleUrls: ['./criteria.component.css']
})
export class CriteriaComponent implements OnInit, AfterViewInit, OnChanges {
  @ViewChild('filterElement') filterElementRef: ElementRef;
  @Input() displayDetail: boolean
  @Input() hitsCounter: number
  @Output() valueChange: EventEmitter<string> =
    new EventEmitter<string>();

  hitsMessage: string;
  filterMaxLength: number;
  // listFilter: string;

  private _ListFilter: string;

  get listFilter(): string {
    return this._ListFilter;
  }

  set listFilter(value: string) {
    this._ListFilter = value;
    this.valueChange.emit(this._ListFilter);
  }


  constructor() { }

  ngOnInit() {
    this.filterMaxLength = 30;
  }

  ngAfterViewInit(): void {
    let element = this.filterElementRef.nativeElement;
    if (element)
      element.focus();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['hitsCounter'] && !changes['hitsCounter'].currentValue) {
      this.hitsMessage = 'No Matches Found';
    } else {
      this.hitsMessage = `Hits: ${this.hitsCounter}`;
    }

  }

}
